from django.contrib import admin

from .models import TodoList, TodoItem


# # Register your models here.
class TodoListAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoList)


class TodoItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoItem)
