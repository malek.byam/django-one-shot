from django.shortcuts import render, redirect

from django.conf import settings
from .models import TodoList

# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/todos.html", context)
